# Archgate Server Administration Manual
This document describes how to configure and compile Archgate
for the first time. It also describes how to administer the
server including documentation of command line parameters,
how to interpret system logs and a general description of how
to maintain the server.

## Contents
1. Introduction

    1. Background
    2. Are you sure about this?

2. Getting Started

    1. System Requirements
    2. Downloading and unpacking the source
    3. Configuring the build system
    4. Compiling Archgate

3. Running the Server

    1. Execution and Systemd
    2. Command Line Options


## 1. Introduction
### 1.1 Background
Archgate is an idea for a multi-user dungeon that a couple
college students decided would be their next big project.
A basic implementation based on tbaMUD was soon developed to
run on Linux servers. The base engine is written in C with a
focus on POSIX specific enhancements, such as POSIX Threads.

### 1.2 Are you sure about this?
If you have experience playing MUDs, then you should know that
it comes with a modicum of responsibility: you have to monitor
in-game chatlogs, maintain the server the game is running on,
constantly be nagged about feature enhancements and such and the
list goes on and on. This picture is not all gloom and doom,
however; and it is a reasonable amount of fun! For every bad
player, there are always ten good ones that make life easier for
everyone involved.

## 2. Getting Started
### 2.1 System Requirements
Archgate is Linux/Unix software, and it will run on most Unix-like
systems without much hassle. If you want to run it on a Windows
machine, there are some additional steps to be taken: you must
first find a POSIX thread implementation for Windows systems.
Some examples include:

* [Cygwin](https://cygwin.com/)
* [pthread-win32](https://sourceware.org/pthreads-win32/)
* [MinGW MSYS](http://mingw.org/wiki/msys)

### 2.2 Downloading and unpacking the source
This section is mostly for distribution maintainers and
developers, but can be useful for anyone.

Visit the [Gitlab](https://www.gitlab.com/gymlidanny/archgate)
repository of the project, download the source or clone it via
`git clone <URL>`. If you cloned it, you're done; just `cd` into
the project tree and continue on. If you downloaded it, just run
`tar -xvf <project tarball>`.

### 2.3 Configuring the build system
Run `CMake -G "Unix Makefiles"` and `make` to get a working copy.
If you have an IDE, such as Visual Studio, take a look at the
[CMake documentation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html)
for more information about how to generate a valid project file
for your environment.

### 2.4 Compiling Archgate
Run `make`.

Again, if you don't have access to Unix-style makefiles, read the
CMake documentation for your specific case.

## 3. Running the Server
### 3.1 Execution and Systemd
To run the server just execute `archgate`. It will read the
config file at `/etc/archgate/archgate.conf` if it exists and
will simply error if it doesn't. You can specify a different path
for the config with command line option `-c`.
Usually, the distribution maintainer will have an init script or
a systemd unit file for your use. In that case, just run
`systemctl start archgate.service`. The unit file will specify
where the config file is.

### 3.2 Command line Options
WIP
