# Archgate
Multi-user dungeon server/client written in pure C. All
documentation is in the `doc/` directory.

## System requirements

### Server
* Unix-like operating system (Linux/BSD/macOS)
* POSIX threads

## Compiling
To compile on Linux/BSD/macOS just clone the repository by running:
```
git clone https://gitlab.com/gymlidanny/archgate.git
cd archgate
mkdir build
cd build
cmake -G "Unix Makefiles" ..
```
