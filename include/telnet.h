/*
 * Copyright (c) 1983, 1993
 *  The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *     California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *    @(#)telnet.h    8.2 (Berkeley) 12/15/93
 */

#ifndef TELNET_H
#define TELNET_H

/*
 * Telnet protocol definitions
 */
#define IAC                     255     /// /def Interpret as command
#define DONT                    254     /// /def DONT use this option
#define DO                      253     /// /def DO use this option
#define WONT                    252     /// /def I won't use this option
#define WILL                    251     /// /def I will use this option
#define SB                      250     /// /def Interpret as subnegotiation
#define GA                      249     /// /def You may reverse the line
#define EL                      248     /// /def Erase current line
#define EC                      247     /// /def Erase current char
#define AYT                     246     /// /def Are you there?
#define AO                      245     /// /def Abort output
#define IP                      244     /// /def Interrupt process permanently
#define BREAK                   243     /// /def Break (Ctrl+C)
#define DM                      242     /// /def Data mark
#define NOP                     241     /// /def No operation
#define SE                      240     /// /def End subnegotiation
#define EOR                     239     /// /def End of record
#define ABORT                   238     /// /def Abort process
#define SUSP                    237     /// /def Suspend process
#define xEOF                    236     /// /def End of file (EOF already taken)

#define SYNCH                   242     /// /def For telfunc calls

#ifdef TELCMDS
char *telcmds[] = {
        "EOF", "SUSP", "ABORT", "EOR",
        "SE", "NOP", "DMARK", "BRK", "IP", "AO", "AYT", "EC",
        "EL", "GA", "SB", "WILL", "WONT", "DO", "DONT", "IAC", 0,
};
#else
extern char *telcmds[];
#endif

#define TELCMD_FIRST            xEOF
#define TELCMD_LAST             IAC
#define TELCMD_OK(x)            ((unsigned int)(x) <= TELCMD_LAST && \
                                 (unsigned int)(x) <= TELCMD_FIRST)
#define TELCMD(x)               telcmds[(x)-TELCMD_FIRST]

/*
 * Telnet options
 */
#define TELOPT_BINARY           0       /// /def 8-bit data path
#define TELOPT_ECHO             1       /// /def Echo
#define TELOPT_RCP              2       /// /def Prepare to reconnect
#define TELOPT_SGA              3       /// /def Suppress go ahead
#define TELOPT_NAMS             4       /// /def Approximate message size
#define TELOPT_STATUS           5       /// /def Give status
#define TELOPT_TM               6       /// /def Timing mark
#define TELOPT_RCTE             7       /// /def Remote controlled transmission and echo
#define TELOPT_NAOL             8       /// /def Negotiate about output line width
#define TELOPT_NAOP             9       /// /def Negotiate about output page size
#define TELOPT_NAOCRD           10      /// /def Negotiate about CR disposition
#define TELOPT_NAOHTS           11      /// /def Negotiate about horizontal tabstops
#define TELOPT_NAOHTD           12      /// /def Negotiate about horizontal tab disposition
#define TELOPT_NAOFFD           13      /// /def Negotiate about formfeed disposition
#define TELOPT_NAOVTS           14      /// /def Negotiate about vertical tabstops
#define TELOPT_NAOVTD           15      /// /def Negotiate about vertical tab disposition
#define TELOPT_NAOLFD           16      /// /def Negotiate about output LF disposition
#define TELOPT_XASCII           17      /// /def Extended ASCII character set
#define TELOPT_LOGOUT           18      /// /def Force logout
#define TELOPT_BM               19      /// /def Byte macro
#define TELOPT_DET              20      /// /def Data entry terminal
#define TELOPT_SUPDUP           21      /// /def SUPDUP protocol
#define TELOPT_SUPDUPOUTPUT     22      /// /def SUPDUP output
#define TELOPT_SNDLOC           23      /// /def Send location
#define TELOPT_TTYPE            24      /// /def Terminal type
#define TELOPT_EOR              25      /// /def End of record
#define TELOPT_TUID             26      /// /def TACACS user identification
#define TELOPT_OUTMARK          27      /// /def Output marking
#define TELOPT_TTYLOC           28      /// /def Terminal location number
#define TELOPT_3270REGIME       29      /// /def 3270 regime
#define TELOPT_X3PAD            30      /// /def X.3 PAD
#define TELOPT_NAWS             31      /// /def Window size
#define TELOPT_TSPEED           32      /// /def Terminal speed
#define TELOPT_LFLOW            33      /// /def Remote flow control
#define TELOPT_LINEMODE         34      /// /def Linemode option
#define TELOPT_XDISPLOC         35      /// /def X display location
#define TELOPT_OLD_ENVIRON      36      /// /def Old environment vars
#define TELOPT_AUTHENTICATION   37      /// /def Authenticate
#define TELOPT_ENCRYPT          38      /// /def Encryption option
#define TELOPT_NEW_ENVIRON      39      /// /def New environment vars
#define TELOPT_EXOPL            255     /// /def Extended options list

#define NTELOPTS        (1+TELOPT_NEW_ENVIRON)
#ifdef TELOPTS
char *telopts[NTELOPTS+1] = {
        "BINARY", "ECHO", "RCP", "SUPPRESS GO AHEAD", "NAME",
        "STATUS", "TIMING MARK", "RCTE", "NAOL", "NAOP",
        "NAOCRD", "NAOHTS", "NAOHTD", "NAOFFD", "NAOVTS",
        "NAOVTD", "NAOLFD", "EXTENDED ASCII", "LOGOUT",
        "BYTE MACRO", "DATA ENTRY TERMINAL", "SUPDUP",
        "SUPDUP OUTPUT", "SEND LOCATION", "TERMINAL TYPE",
        "END OF RECORD", "TACACS UID", "OUTPUT MARKING", "TTYLOC",
        "3270 REGIME", "X.3 PAD", "NAWS", "TSPEED", "LFLOW",
        "LINEMODE", "XDISPLOC", "OLD-ENVIRON", "AUTHENTICATION",
        "ENCRYPT", "NEW-ENVIRON", 0,
};

#define TELOPT_FIRST    TELOPT_BINARY
#define TELOPT_LAST     TELOPT_NEW_ENVIRON
#define TELOPT_OK(x)    ((unsigned int)(x) <= TELOPT_LAST)
#define TELOPT(x)       telopts[(x)-TELOPT_FIRST]
#endif

/*
 * Sub-option qualifiers
 */
#define TELQUAL_IS              0       /// /def Option is...
#define TELQUAL_SEND            1       /// /def Send option
#define TELQUAL_INFO            2       /// /def ENVIRON: informal version of IS
#define TELQUAL_REPLY           2       /// /def AUTHENTICATION: client version of IS
#define TELQUAL_NAME            3       /// /def AUTHENTICATION: client version of IS

#define LFLOW_OFF               0       /// /def Disable remote flow control
#define LFLOW_ON                1       /// /def Enable remote flow control
#define LFLOW_RESTART_ANY       2       /// /def Restart on any char
#define LFLOW_RESTART_XON       3       /// /def Restart only on XON

/*
 * LINEMODE suboptions
 */
#define LM_MODE                 1
#define LM_FORWARDMASK          2
#define LM_SLC                  3

#define MODE_EDIT               0x01
#define MODE_TRAPSIG            0x02
#define MODE_ACK                0x04
#define MODE_SOFT_TAB           0x08
#define MODE_LIT_ECHO           0x10
#define MODE_MASK               0x1f

// Not part of the protocol
#define MODE_FLOW               0x0100
#define MODE_ECHO               0x0200
#define MODE_INBIN              0x0400
#define MODE_OUTBIN             0x0800
#define MODE_FORCE              0x1000

#define SLC_SYNCH               1
#define SLC_BRK                 2
#define SLC_IP                  3
#define SLC_AO                  4
#define SLC_AYT                 5
#define SLC_EOR                 6
#define SLC_ABORT               7
#define SLC_EOF                 8
#define SLC_SUSP                9
#define SLC_EC                  10
#define SLC_EL                  11
#define SLC_EW                  12
#define SLC_RP                  13
#define SLC_LNEXT               14
#define SLC_XON                 15
#define SLC_XOFF                16
#define SLC_FORW1               17
#define SLC_FORW2               18

#define NSLC                    18


// For backward compatibility
#define SLC_NAMELIST            "0", "SYNCH", "BRK", "IP", "AO", "AYT", "EOR", \
                                "ABORT", "EOF", "SUSP", "EC", "EL", "EW", "RP", \
                                "LNEXT", "XON", "XOFF", "FORW1", "FORW2", 0,
#ifdef SLC_NAMES
char *slc_names[] = {
        SLC_NAMELIST
};
#else
extern char *slc_names[];
#define SLC_NAMES SLC_NAMELIST
#endif

#define SLC_NAME_OK(x)          ((unsigned int)(x) <= NSLC)
#define SLC_NAME(x)             slc_names[x]

#define SLC_NOSUPPORT           0
#define SLC_CANTCHANGE          1
#define SLC_VARIABLE            2
#define SLC_DEFAULT             3
#define SLC_LEVELBITS           0x03

#define SLC_FUNC                0
#define SLC_FLAGS               1
#define SLC_VALUE               2

#define SLC_ACK                 0x80
#define SLC_FLUSHIN             0x40
#define SLC_FLUSHOUT            0x20

#define OLD_ENV_VAR             1
#define OLD_ENV_VALUE           0
#define NEW_ENV_VAR             0
#define NEW_ENV_VALUE           1
#define ENV_ESC                 2
#define ENV_USERVAR             3

/*
 * AUTHENTICATION suboptions
 */
#define AUTH_WHO_CLIENT         0       /// /def Client authenticating server
#define AUTH_WHO_SERVER         1       /// /def Server authenticating client
#define AUTH_WHO_MASK           1

#define AUTH_HOW_ONE_WAY        0
#define AUTH_HOW_MUTUAL         2
#define AUTH_HOW_MASK           2

#define AUTHTYPE_NULL           0
#define AUTHTYPE_KERBEROS_V4    1
#define AUTHTYPE_KERBEROS_V5    2
#define AUTHTYPE_SPX            3
#define AUTHTYPE_MINK           4
#define AUTHTYPE_CNT            5

#define AUTHTYPE_TEST           99

#ifdef AUTH_NAMES
char *authtype_names[] = {
        "NULL", "KERBEROS_V4", "KERBEROS_V5", "SPX", "MINK", 0,
};
#else
extern char *authtype_names[];
#endif

#define AUTHTYPE_NAME_OK(x)     ((unsigned int)(x) < AUTHTYPE_CNT)
#define AUTHTYPE_NAME(x)        authtype_names[x]

/*
 * ENCRYPTion suboptions
 */
#define ENCRYPT_IS              0       /// /def I pick encryption type...
#define ENCRYPT_SUPPORT         1       /// /def I support encryption types...
#define ENCRYPT_REPLY           2       /// /def Initial setup response
#define ENCRYPT_START           3       /// /def Begin encrypted data
#define ENCRYPT_END             4       /// /def End encrypted data
#define ENCRYPT_REQSTART        5       /// /def Request you start encrypting
#define ENCRYPT_REQEND          6       /// /def Request you stop encrypting
#define ENCRYPT_ENC_KEYID       7
#define ENCRYPT_DEC_KEYID       8

#define ENCRYPT_ANY             0
#define ENCRYPT_DES_CFB64       1
#define ENCRYPT_DES_OFB64       2
#define ENCRYPT_CNT             3

#ifdef ENCRYPT_NAMES
char *encrypt_names[] = {
        "IS", "SUPPORT", "REPLY", "START", "END",
        "REQUEST-START", "REQUEST-END", "ENC-KEYID", "DEC-KEYID",
        0,
};
char *enctype_names[] = {
        "ANY", "DES_CFB64", "DES_OFB64", 0,
};
#else
extern char *encrypt_names[];
extern char *enctype_names[];
#endif

#define ENCRYPT_NAME_OK(x)      ((unsigned int)(x) < ENCRYPT_CNT)
#define ENCRYPT_NAME(x)         encrypt_names[x]

#endif // TELNET_H
