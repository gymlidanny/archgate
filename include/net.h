/** \file net.h
 *
 * Defines functions and structs needed for network communication.
 */

#ifndef NET_H
#define NET_H

#include "structs.h"
#include <stdlib.h>
#include <stdarg.h>

// Global variables (NOT THREAD SAFE)
extern int buf_largecount;
extern int buf_overflows;
extern int buf_switches;

/**
 * @struct net_args
 * @brief Arguments passed into net_thread_net
 *
 * This struct contains all the server information that net_init
 * requires to initialize the networking stack.
 */
struct net_args {
        int port;       /* !< Network port number. */
        int *sockets;   /* !< Array of socket descriptors. */
        int plimit;     /* !< Player limit. */
};

/// client_init
/**
 * The client_init function is a client-specific function that
 * calls connect() with a user-specified port and address, and
 * then initializes encrypted communication to the server. It
 * returns only a single socket.
 *
 * @param Pointer to const char containing server address
 * @param Integer containing the port number, passing 0 means
 * use defaults
 */
int client_init(const char *addr, int port);

/// client_close
/**
 * The client_close function is a client-specific function that
 * closes the socket descriptor 'sock', does cleanup of the
 * session keys and stores the client keypair in a file.
 *
 * @param Integer containing the client's socket descriptor
 */
void client_close(int sock);

/// net_init
/**
 * The net_init function is a server-specific function that
 * initializes the socket descriptor array and starts listening
 * for client connections. This function creates plimit-1 sockets
 * and will not return until the listening socket dies.
 *
 * @param Integer containing the port number
 * @param Null pointer to array of socket descriptors
 * @param Integer containing the player limit
 *
 * @return 0 on success, -1 on error
 */
int net_init(int port, int *socks, int plimit);

/// net_close
/**
 * The net_close function is a server-specific function that
 * closes all sockets in the socks array and does general
 * cleanup of the server's networking stack.
 *
 * @param Array of socket descriptors
 * @param Player limit
 */
void net_close(int *socks, int plimit);

/// net_thread_init
/**
 * The net_thread_init function calls net_init function in its
 * own thread. This function will not return until net_init returns.
 *
 * @param Pointer to struct net_args
 */
void* net_thread_init(void *args);

/// handshake
/**
 * The hanshake function is a server-specific function that runs
 * in its own thread and authenticates users. This function is
 * guaranteed to be thread safe.
 *
 * @param Pointer to integer cast as void of a socket descriptor
 */
void* handshake(void *sock);

/// write_to_buffer
/**
 * The write_to_buffer function writes a const char to a
 * specified buffer. This buffer can be a descriptor_data_t
 * or a file. Returns number of bytes written.
 *
 * @param Pointer to descriptor_data_t
 * @param Array of char that is the message to write
 */
size_t write_to_buffer(struct descriptor_data *t, const char *txt, ...) __attribute__ ((format (printf, 2, 3)));

/// vwrite_to_buffer
/**
 * The vwrite_to_buffer function writes a const char to a
 * specified buffer. This buffer can be a descriptor_data_t
 * or a file. Returns number of bytes written.
 *
 * @param Pointer to descriptor_data_t
 * @param Formatted string message to write
 * @param Argument list
 */
size_t vwrite_to_buffer(struct descriptor_data *t, const char *format, va_list args);

#endif
