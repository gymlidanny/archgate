/** \file config.h
 *
 * Configuration options from argv and config file.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "structs.h"

// Global variables
extern struct config_data config_info;

// Configuration macros pulled from config_data
#define CONFIG_CONFFILE config_info.CONFFILE
#define CONFIG_PK_ALLOWED config_info.play.pk_allowed
#define CONFIG_PT_ALLOWED config_info.play.pt_allowed
#define CONFIG_PORT config_info.port
#define CONFIG_PLIMIT config_info.plimit
#define CONFIG_VERBOSE config_info.verbose

/// read_config
/**
 * The read_config function looks for the global configuration
 * file at 'filename'. It reads the file and redefines the above
 * macros if needed. This function returns void.
 *
 * @param Array of char that describes where the config file is
 */
void read_config(const char *filename);

#endif
