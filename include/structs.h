/** \file structs.h
 *
 * Required data structures used throughout the engine
 */

#ifndef STRUCTS_H
#define STRUCTS_H

#include "protocol.h"
#include <stdlib.h>
#include <pthread.h>

// Cardinal and relative directions
#define NORTH                   0       /// /def Cardinal North
#define EAST                    1       /// /def Cardinal East
#define SOUTH                   2       /// /def Cardinal South
#define WEST                    3       /// /def Cardinal West
#define UP                      4       /// /def Relative up
#define DOWN                    5       /// /def Relative down
#define NORTHWEST               6       /// /def Intercardinal NW
#define NORTHEAST               7       /// /def Intercardinal NE
#define SOUTHEAST               8       /// /def Intercardinal SE
#define SOUTHWEST               9       /// /def Intercardinal SW

#define NUM_OF_DIRS             10      /// /def Total number of possible directions

// Room flags
#define ROOM_DARK               0       /// /def Dark room, light needed
#define ROOM_DEATH              1       /// /def Death trap, instant death
#define ROOM_NOMOB              2       /// /def MOBs not allowed in this room
#define ROOM_INDOORS            3       /// /def Indoors, no weather
#define ROOM_PEACEFUL           4       /// /def Violence not allowed
#define ROOM_SOUNDPROOF         5       /// /def Shouts, gossip blocked
#define ROOM_NOTRACK            6       /// /def Track won't go through
#define ROOM_NOMAGIC            7       /// /def Magic not allowed
#define ROOM_TUNNEL             8       /// /def Room for only 1 entity
#define ROOM_PRIVATE            9       /// /def Can't teleport in
#define ROOM_GODROOM            10      /// /def LVL_GOD+ only allowed
#define ROOM_HOUSE              11      /// /def Room is a house
#define ROOM_HOUSE_CRASH        12      /// /def House needs saving
#define ROOM_ATRIUM             13      /// /def House entryway
#define ROOM_OLC                14      /// /def Room is modifyable
#define ROOM_BFS_MARK           15      /// /def Breath-first search mark
#define ROOM_WORLDMAP           16      /// /def Worldmap-style maps

#define NUM_ROOM_FLAGS          17      /// /def Total number of room flags

// History
#define HIST_ALL                0       /// /def History of all channels
#define HIST_SAY                1       /// /def History of all 'say'
#define HIST_GOSSIP             2       /// /def History of all 'gossip'
#define HIST_WIZNET             3       /// /def History of all 'wiznet'
#define HIST_TELL               4       /// /def History of all 'tell'
#define HIST_SHOUT              5       /// /def History of all 'shout'
#define HIST_GRATS              6       /// /def History of all 'grats'
#define HIST_HOLLER             7       /// /def History of all 'holler'
#define HIST_AUCTION            8       /// /def History of all 'auction'

#define NUM_HIST                9       /// /def Total number of history indexes
#define HIST_SIZE               5       /// /def Number of last commands kept in each history

// System macros
#define MAX_SOCK_BUF            1048    /// /def Size of kernel's sock buf
#define MAX_PROMPT_LENGTH       96      /// /def Max length of prompt
#define GARBAGE_SPACE           32      /// /def Space for **OVERFLOW** etc
#define SMALL_BUFSIZE           1024    /// /def Static output buffer size
#define LARGE_BUFSIZE           (MAX_SOCK_BUF - GARBAGE_SPACE - MAX_PROMPT_LENGTH)

#define MAX_STRING_LENGTH       49152   /// /def Max length of string
#define MAX_INPUT_LENGTH        512     /// /def Max length per line of input
#define MAX_RAW_INPUT_LENGTH    1036    /// /def Max size of raw input
#define MAX_MESSAGES            60      /// /def Max Different attack message types
#define MAX_NAME_LENGTH         20      /// /def Max PC/NPC name length
#define MAX_PWD_LENGTH          30      /// /def Max PC password length
#define MAX_TITLE_LENGTH        80      /// /def Max PC title length
#define HOST_LENGTH             40      /// /def Max hostname resolution length
#define PLR_DESC_LENGTH         4096    /// /def Max length for PC description
#define MAX_SKILLS              200     /// /def Max number of skills/spells
#define MAX_AFFECT              32      /// /def Max number of player affections
#define MAX_OBJ_AFFECT          6       /// /def Max object affects
#define MAX_NOTE_LENGTH         4000    /// /def Max length of text on a note obj
#define MAX_LAST_ENTRIES        6000    /// /def Max log entries
#define MAX_HELP_KEYWORDS       256     /// /def Max length of help keyword string
#define MAX_HELP_ENTRY          MAX_STRING_LENGTH /// /def Max length of help entry
#define MAX_COMPLETED_QUESTS    1024    /// /def Max number of completed quests allowed

#define MAX_GOLD                2140000000 /// /def Max possible gold on hand
#define MAX_BANK                2140000000 /// /def Max possible gold in bank account

#define MAX_CMD_LENGTH          16384

// Type definitions
typedef signed char sbyte;      /// /typedef Single byte ranging -127 to 127
typedef unsigned char ubyte;    /// /typedef Single byte ranging 0 to 255
typedef signed short sh_int;    /// /typedef Two bytes (-32,768 to 32,767)
typedef unsigned short ush_int; /// /typedef Two bytes (0 to 65535)
#ifndef __cplusplus
typedef char bool;
#endif
#ifndef LCC_WIN32
typedef signed char byte;
#endif

/**
 * @struct game_data
 * @brief Master game setting structure
 *
 * This structure stores the in-game settings set by the
 * administrator.
 */
struct game_data {
        bool pk_allowed;                ///< Is player killing allowed?
        bool pt_allowed;                ///< Is player thieving allowed?
        bool level_can_shout;           ///< Level player must be to shout
        int tunnel_size;                ///< Number of people allowed in a tunnel
        int max_exp_gain;               ///< Max number of experience points per kill
        int max_exp_loss;               ///< Max number of experience points per death
        int max_npc_corpse_time;        ///< Number of ticks before NCP corpses despawn
        int max_pc_corpse_time;         ///< Number of ticks before PC corpses despawn
        int idle_void;                  ///< Number of ticks before PC sent to the void
        int idle_max_level;             ///< Level of players immune to idle
        int dts_are_dumps;              ///< Should items in dt's be junked?
        int load_into_inventory;        ///< Objects load in immortals memory
        int track_through_doors;        ///< Track through doors while closed?
        int no_mort_to_immort;          ///< Prevent mortals leveling to immortals?
        int disp_closed_doors;          ///< Display closed doors in autoexit?
        int diagonal_dirs;              ///< Are there 6 or 10 directions?
        int map_option;                 ///< MAP_ON, MAP_OFF or MAP_IMM_ONLY?
        int map_size;                   ///< Default size for map command
        int minimap_size;               ///< Default size for minimap
        int script_players;             ///< Is attaching scripts to players allowed?

        char *OK;                       ///< 'Okay.'
        char *HUH;                      ///< 'Huh?!'
        char *NOPERSON;                 ///< 'No person by that name here.'
        char *NOEFFECT;                 ///< 'Nothing seems to happen.'
        pthread_mutex_t write_lock;     ///< This structure is mutable only when unlocked
};

/**
 * @struct crash_save_data
 * @brief What the game does on crash
 *
 * This structure describes what the engine should do when it crashes
 * and the operating system sends a kill signal.
 */
struct crash_save_data {
        bool auto_save;                 ///< Does the game automatically save people?
        int auto_save_time;             ///< If auto_save == true, how often?
        int crash_file_timeout;         ///< Life of crashfiles and idlesaves
};

/**
 * @struct txt_block
 * @brief Thread safe string implementation
 *
 * This struct 
 */
struct txt_block {
        char *text;                     ///< The string
        int aliased;
        struct txt_block *next;         ///< Next element in the linked list
};

/**
 * @struct txt_q
 * @brief Linked list of txt_blocks
 *
 * This struct contains a linked list of all in-game txt_blocks.
 */
struct txt_q {
        struct txt_block *head;         ///< Head of the linked list
        struct txt_block *tail;         ///< Tail of the linked list
        pthread_mutex_t write_lock;     ///< This structure is mutable only when unlocked
};

/**
 * @struct descriptor_data
 * @brief Master socket descriptor structure
 *
 * This struct contains the master descriptor for each player.
 */
struct descriptor_data {
        int descriptor;                 ///< File descriptor for socket
        char host[HOST_LENGTH+1];       ///< Hostname
        byte bad_pws;                   ///< Number of bad password attempts
        sh_int connected;               ///< Mode of connectedness
        int desc_num;                   ///< Unique number assigned to desciptor
        time_t login_time;              ///< Time of intial connection
        char *showstr_head;             ///< Keeping track of an internal string
        char **showstr_vector;          ///< For paging through texts
        int showstr_count;              ///< Number of pages to page through
        int showstr_page;               ///< Which one are we showing?
        char **str;                     ///< For the modify-str system
        char *backstr;                  ///< Backup string for modify-str
        size_t max_str;                 ///< Max size of string in modify-str
        long mail_to;                   ///< Name for mail system
        int has_prompt;                 ///< Is user at a prompt?
        char inbuf[MAX_RAW_INPUT_LENGTH]; ///< Buffer for raw input
        char last_input[MAX_INPUT_LENGTH]; ///< The last input
        char small_outbuf[SMALL_BUFSIZE]; ///< Standard output buffer
        char *output;                   ///< Pointer to current buffer
        char **history;                 ///< History of commands
        int history_pos;                ///< Circular array position
        int bufptr;                     ///< Pointer to end of current output
        int bufspace;                   ///< Space left in output buffer
        struct txt_block *large_outbuf; ///< Pointer to large buffer
        struct txt_q input;             ///< Queue of unprocessed input
        //struct char_data *character;    ///< Linked to char
        //struct char_data *original;     ///< Original char if switched
        struct descriptor_data *snooplog; ///< Who is this char snooping?
        struct descriptor_data *snoop_by; ///< Who is snooping this char?
        struct descriptor_data *next;   ///< Link to next descriptor
        struct oasis_olc_data *olc;
        protocol_t *pProtocol;          ///< Required by Kavir snippet
        struct list_data *events;

        pthread_mutex_t write_lock;     ///< This structure is mutable only when unlocked
};

/*
 * Main configuration structure
 */
struct config_data {
        const char *CONFFILE;           ///< Path to whitespace delimited config file
        struct game_data play;          ///< In-game global settings
        struct crash_save_data csd;     ///< How crash files and such are handled
        int port;                       ///< TCP/IP portnum
        int plimit;                     ///< Player limit
        bool verbose;                   ///< Is verbose output enabled?
        pthread_mutex_t write_lock;     ///< This structure is mutable only when unlocked
};

#endif // STRUCT_H
