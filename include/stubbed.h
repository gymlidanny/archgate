#ifndef STUBBED_H
#define STUBBED_H

#include <stdio.h>

#define STUBBED(x) do {\
        static int seen_this = 0;\
        if (seen_this == 0) {\
                seen_this = 1;\
                fprintf(stderr, "STUBBED: %s at %s (%s:%d)\n",\
                                x, __FUNCTION__, __FILE__, __LINE__);\
        }\
} while(0)

#endif
