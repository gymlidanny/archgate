#include "config.h"
#include "net.h"
#include "getopt.h"
#include "pthread.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
        int opt = 0;
        while ((opt = getopt(argc, argv, "c:vp:l:") != -1)) {
                switch (opt) {
                        case 'c':
                                CONFIG_CONFFILE = optarg;
                                break;
                        case 'v':
                                CONFIG_VERBOSE = true;
                                break;
                        case 'p':
                                CONFIG_PORT = atoi(optarg);
                                break;
                        case 'l':
                                CONFIG_PLIMIT = atoi(optarg);
                                break;
                }
        }

        struct net_args *args = malloc(sizeof(struct net_args));
        args->port = CONFIG_PORT;
        args->plimit = CONFIG_PLIMIT;
        if (args->port == 0)
                args->port = 1234;
        if (args->plimit == 0)
                args->plimit = 5;
        pthread_t listen_thread;
        if (pthread_create(&listen_thread, NULL, net_thread_init, (void*)args) < 0) {
                perror(argv[0]);
                exit(EXIT_FAILURE);
        }

        int *err;
        pthread_join(listen_thread, (void*)&err);
        free(args);
        return 0;
}
