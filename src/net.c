#include "net.h"
#include "pthread.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

int net_init(int port, int *socks, int plimit) {
        socks = malloc(sizeof(int)*(plimit+1));
        printf("Initializing networking stack\n");
        struct sockaddr_storage client;
        struct addrinfo hints;
        struct addrinfo *res;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;
        char str[12];
        sprintf(str, "%d", port);
        int err = getaddrinfo(NULL, str, &hints, &res);
        if (err != 0)
                return err;
        socks[plimit] = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        int enable = 1;
        err = setsockopt(socks[plimit], SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
        if (err != 0)
                return err;
        if (socks[plimit] == -1) {
                perror("Socket");
                return -1;
        }
        if (bind(socks[plimit], res->ai_addr, res->ai_addrlen) == -1) {
                perror("Socket");
                return -1;
        }
        listen(socks[plimit], 10);
        printf("Server listening on port %d\n", port);

        pthread_t accept_thread;
        socklen_t addr_size = sizeof(client);
        for (int i = 0; i < plimit; i++) {
                while ((socks[i] = accept(socks[plimit], (struct sockaddr*)&client, &addr_size)) != -1) {
                        pthread_t sniffer_thread;
                        if (pthread_create(&sniffer_thread, NULL, handshake, (void*)&socks[i]) < 0) {
                                perror("Thread");
                                return -1;
                        }
                }
        }
        return 0;
}

void net_close(int *socks, int plimit) {
        if (socks != NULL) {
                for (int i = 0; i < plimit; i++)
                        close(socks[i]);
                free(socks);
        }
}

void* net_thread_init(void *args) {
        struct net_args *arglist = args;
        int err = net_init(arglist->port, arglist->sockets, arglist->plimit);
        if (err != 0)
                pthread_exit(&err);
        net_close(arglist->sockets, arglist->plimit);
}

void* handshake(void *sock) {
        pthread_detach(pthread_self());
        int sock_desc = *(int*)sock;
        struct sockaddr_in addr;
        socklen_t addr_len = sizeof(addr);
        if (getpeername(sock_desc, (struct sockaddr*)&addr, &addr_len) != 0)
                perror("Failed to retrive client IP");
        char str[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &addr.sin_addr, str, INET_ADDRSTRLEN);
        printf("New connection from %s\n", str);
        int read_size;
        char *msg;
        char client_message[2000];

        msg = "ARCHGATE MUD SERVER\n";
        send(sock_desc, msg, strlen(msg), 0);

        msg = "BEGIN HANDSHAKE\n";
        send(sock_desc, msg, strlen(msg), 0);

        int loop = 1;
        unsigned char *client_pk;
        while (loop == 1) {
                read_size = recv(sock_desc, client_message, 2000, 0);
                memset(client_message, 0, 2000);
        }
        if (read_size == -1)
                perror("Handshake error");
        else if (read_size == 0) //Connection closed
                return (void*)0;
        return (void*)sock;
}
