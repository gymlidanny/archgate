#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct config_data config_info;

void read_config(const char *filename) {
        pthread_mutex_lock(&config_info.write_lock);
        if (filename != NULL)
                CONFIG_CONFFILE = filename;
        // TODO: implement this
        pthread_mutex_unlock(&config_info.write_lock);
}
